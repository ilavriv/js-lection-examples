import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import BooksAPI from '../../modules/books'


const BooksPage = (props) => <BooksAPI.components.Books {...props} />;

const mapStateToProps = createStructuredSelector({
  books: BooksAPI.selectors.getAllBooks
});

export default connect(mapStateToProps)(BooksPage)