import { combineReducers } from 'redux';
import books from './modules/books/reducers';

const rootReducer = combineReducers({
  books
});

export default rootReducer;