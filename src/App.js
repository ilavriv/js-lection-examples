import React from 'react';
import logo from './logo.svg';
import './App.css';

import { Provider } from 'react-redux';

import store from './store';

import Books from './pages/Books';

function App() {
  return (
    <Provider store={store}>
      <Books />
    </Provider>
  );
}

export default App;
