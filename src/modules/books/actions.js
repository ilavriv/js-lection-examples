import { createActions } from 'redux-actions';

const booksActions = createActions({
  FETCH: ({ isLoading }) => true,
  SEUCCESS: ({ data }) => ({ data, isLoading: false })
});

export default booksActions

