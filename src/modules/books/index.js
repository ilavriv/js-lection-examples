import * as components from './components';
import * as selectors from './selectors';

export default {
  components,
  selectors
}

