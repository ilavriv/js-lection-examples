import React from 'react';


export default ({ books }) => <div>{books.map((b, i) => <div key={i}>{b.name}</div>)}</div>
