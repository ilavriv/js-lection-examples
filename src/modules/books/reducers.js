import { combineReducers } from 'redux';

const bookListReducer = (state = [{
  name: 'Test book'
}], action) => {
  return state;
}

const activeBooksReducer = (state = {}, action) => {
  return state;
}

export default combineReducers({
  bookListReducer,
  activeBooksReducer
});